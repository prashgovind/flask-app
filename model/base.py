from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from config import properties

engine = create_engine('mysql+pymysql://' + properties['database.username'] + ':'
                       + properties['database.password'] + '@' + properties['database.url'])
Session = sessionmaker(bind=engine)

Base = declarative_base()
