from flask import Flask

from controller.greeter_controller import greeter_api
from controller.user_controller import user_api

app = Flask(__name__)
app.register_blueprint(greeter_api)
app.register_blueprint(user_api)

if __name__ == "__main__":
    app.run()
