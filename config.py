import requests
import json
import os
import sys


def get_argument_or_default(name, default):
    app_arg = list(filter(lambda arg: arg.startswith('--' + name + '='), sys.argv))
    return app_arg[0].split('=')[1] if len(app_arg) > 0 and len(app_arg[0].split('=')[1]) > 0 else default


def get_config():
    config_server_url = 'user:' + os.environ['CONFIGSERVER_PASSWORD'] + '@' + os.environ['CONFIGSERVER_NAME']
    active_profiles = get_argument_or_default('profiles', 'default')
    print('Fetching config from ' + config_server_url + '. Profiles=' + active_profiles)
    config_props = {}
    response = requests.get('http://' + config_server_url
                            + '/' + get_argument_or_default('app.name', 'flask-sample-app') + '/' + active_profiles)
    config = json.loads(response.text)
    property_sources = config['propertySources']
    for propertySource in property_sources:
        print('Located property source: ' + propertySource['name'])
        props = propertySource['source']
        for key, val in props.items():
            config_props[key] = val
    print('Finished loading config')
    return config_props


properties = get_config()
