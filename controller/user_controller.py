from flask import Blueprint, jsonify
from service.user_service import UserService

user_api = Blueprint('user_api', __name__)
user_service = UserService()


@user_api.route('/users')
def get_all():
    users = user_service.get_all()
    return jsonify([user.serialize() for user in users])


@user_api.route('/users/<user_id>')
def get_user(user_id):
    user = user_service.get_by_id(user_id)
    return jsonify(user.serialize())


@user_api.route('/save-user', methods=['POST'])
def save_user():
    user_service.save("ABC", "DEF")
    return "{}"
