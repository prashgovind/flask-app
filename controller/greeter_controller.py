from flask import Blueprint
from config import properties

greeter_api = Blueprint('greeter_api', __name__)


@greeter_api.route('/greet')
def greet():
    try:
        greeting = properties['app.greeting']
    except KeyError:
        greeting = "Hello World!"
    return greeting
