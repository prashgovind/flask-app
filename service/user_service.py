from model.base import Session
from model.user import User


class UserService:
    session = Session()

    def get_all(self):
        return self.session.query(User).all()

    def get_by_id(self, user_id):
        return self.session.query(User) \
            .filter(User.user_id == user_id) \
            .first()

    def save(self, firstName, lastName):
        self.session.add(User(first_name=firstName, last_name=lastName))
        self.session.commit()
